title: Vannerie au jardin
<<: !!inc/file ../dates.yml
eventFilterSlug: vannerie
-------------------
# Vannerie au jardin

<img src="img/vannerie3.jpg" class="portrait"/>

Nous nous retrouverons dans un jardin collectif Près de Saucats (33), pour apprendre le tressage des bordures ou des rames, ainsi que la confection d’objets utiles ou décoratifs.<br>
Nous utiliserons de l’osier ou des rameaux de différentes essences prélevés dans les haies à tailler.<br>
L’atelier que je vous propose est une initiation au vastes possibilités de la vannerie, pour aller plus loin, vous pourrez contacter un professionnel de cette discipline.

<br>
<br>
<br>
<br>

<a class="button right withPicto agenda" style="margin-right: 30px" href="dates.html">Prochaines Dates...</a>
