title: Classes Nature
<<: !!inc/file ../dates.yml
eventFilterSlug: Classes
-------------------
# Classes Nature

<img src="img/graines1.jpg" class="portrait"/>

Nos animations peuvent agrémenter une journée festive ou une manifestation ouverte au public dont le thème se rapporte à la nature, au jardinage permaculturel, à l’écologie...<br>
Voici les principales animations déjà présentées lors de journées à thème :
- le vivant dans le sol : présentation des différents acteurs du sol qui participent à sa fertilité
- l'histoire de l'apparition du végétal sur terre, son évolution jusqu'à nos jours
- les familles botaniques au jardin : corrélation entre plantes cultivées et sauvages
- la fleur expliquée par un jeu
- les graines véhicules de vie, de toutes formes et d'ingéniosité




<br>
<br>
<br>
<br>
<br>
<br>

# Galerie photo

<br>

<%
const htmlAlbum = [];
htmlAlbum.push(`<div class="card ratio-3-4 avecTitre"><img src="img/galeries/evolution2.jpg"/><span></span></div>`);
htmlAlbum.push(`<div class="card ratio-3-4 avecTitre"><img src="img/galeries/familles2.jpg"/><span></span></div>`);
htmlAlbum.push(`<div class="card ratio-3-4 avecTitre"><img src="img/galeries/fleurs2.jpg"/><span></span></div>`);
htmlAlbum.push(`<div class="card ratio-3-4 avecTitre"><img src="img/galeries/sol2.jpg"/><span></span></div>`);
%>
<%- include('../../../node_modules/1gallery/1gallery.ejs',{cards:htmlAlbum,classes:'galerieQui'}) %>

