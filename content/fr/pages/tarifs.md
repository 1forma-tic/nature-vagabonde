title: Tarifs
-------------------
# Tarifs

<img src="img/contact.jpg" class="portrait"/>

L'adhésion à l'association (valable un an de date à date) est de :
- 10 euros pour les personnes morales
- 5 euros pour les personnes physiques
- 2 euros pour les étudiants et moins de 18 ans.

Tarifs pour une balade de découverte des plantes sauvages :
- 7 euros pour un adulte
- 3 euros pour un étudiant
- 1 euros pour un enfant

Tarifs pour un atelier de vannerie (durée 3h) :
- 15 euros pour un adulte
- 7 euros pour un étudiant
- 3 euros pour un enfant

Nos tarifs peuvent être adaptés pour les personnes en situation de précarité.

Les adhérents peuvent participer gratuitement aux fêtes des cycles de la nature.

Nous mettons à disposition des adhérents un petit lot de livres sur les thèmes abordés par l’association.
