title: Animations

<<: !!inc/file ../types_de_stages_et_d_evenements.yml
filterByTag: collectif

-------------------

# Animations

<a class="button right withPicto agenda" style="margin-right: 30px" href="dates.html">Prochaines Dates...</a>

Nous proposons :
- des animations sur sites (forêts, prairies, jardins...) afin de vivre et partager une expérience au plus près de la nature.
-des ateliers  itinérants d’apprentissage et d’éveil à la richesse du monde vivant.
<br>Nos animations et ateliers sont accessibles et nous l’espérons enrichissants quel que soit votre âge.

Nous favorisons les approches pédagogiques, légèrement scientifiques, surtout ludiques, artistiques et sensorielles, dans un esprit de convivialité et d'entraide.
<br>Vous avez envie de participer... merci de nous contacter par mail *[voir page liens&contact](contact.md)*.

<br>
<div class="flexArea">
<%
const filteredTypes = filterByTag ? types.filter( (t) => t.tags.indexOf(filterByTag) !== -1 ) : types;
filteredTypes.forEach(t=>{
_%>
<%- include('../fragments/eventTypePresentation.ejs',{evType:t}); %>
<% }); _%>

</div>

<br>

Les animations proposées sont le plus souvent assurées par Hélène.<br>
«C'est un désir de partager mes connaissances, mes sensibilités et les expériences acquises puis régulièrement améliorées qui m'anime.
- Je me suis initiée à la reconnaissance des plantes sauvages comestibles ou non auprès de différents intervenants de Gironde depuis 2007 (Isabelle Lafon, Francine Marque, Jean-Yves Boussereau, Cathy et Dom de l'association Trésors Nature, Martial Theviot de l'association Jardin et écotourisme, Myriam Refay de l'association Icare). 
- J’anime des balades depuis 2014.
- Je me suis formée au tressage du rotin en 1996 et tressage de l’osier en 2005 au près de Daniel Breillat à Nieul-sur-l'Autise. 
- Pour les classes natures j’ai une formation de départ avec des bases de biologie enrichie de recherches autodidacte auprès de bibliographie spécialisées en fonction du sujet.»
