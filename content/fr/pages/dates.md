title: Dates
#https://www.tablesgenerator.com/markdown_tables#

<<: !!inc/file ../dates.yml

-------------------

# Dates à venir

<%
const now = (new Date()).getTime();
const futureEvents = events.filter( (e) => e.dateEnd.getTime()>now );
const pastEvents = events.filter( (e) => e.dateEnd.getTime()<now );
futureEvents.sort( (a,b) => a.dateStart.getTime() - b.dateStart.getTime() );
pastEvents.sort( (a,b) => b.dateStart.getTime() - a.dateStart.getTime() );
futureEvents.forEach(event=>{
_%>
    <%- include('../fragments/event.ejs',{myEvent:event}); %>
<% }); _%>

# Dates passées

<% pastEvents.forEach(event=>{ _%>
    <%- include('../fragments/pastEvent.ejs',{myEvent:event}); %>
<% }); _%>
