title: Anim'Terre
-------------------
# Anim'Terre

<img src="img/terre1.jpg" class="portrait"/>

Avec cette animation, nous proposons un espace de création libre :<br>
avec une récolte de végétaux séchés très divers,<br>
de la terre glaise,<br>
et très peu d’intervention,

c’est l’action de créer qui est favorisée !

<br>
<br>
<br>
<br>

<a class="button right withPicto agenda" style="margin-right: 30px" href="dates.html">Prochaines Dates...</a>
