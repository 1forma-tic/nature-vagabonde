title: Balades de découverte des plantes sauvages comestibles et toxiques
<<: !!inc/file ../dates.yml
eventFilterSlug: ballades
-------------------
# Balades de découverte des plantes sauvages comestibles et toxiques

<img src="img/conference.jpg" class="portrait"/>


Je vous accompagnerai dans la découverte des plantes sauvages comestibles et toxiques et nous aborderons les principes de la cueillette respectueuse et sûre pour les végétaux et pour nous même.<br>
Puis nous partirons en balade à la découverte de la flore sauvage du moment.<br>
Pour finir je vous proposerai quelques recettes possibles en fonction de la récolte.<br>


Pour enrichir votre formation, pratiquez différents accompagnements afin de croiser les connaissances et consolider votre apprentissage. Dans ce but nous vous indiquons d'autres accompagnateurs en Gironde dans la page des [liens&contact](contact.md)*.

<br>
<br>

<a class="button right withPicto agenda" style="margin-right: 30px" href="dates.html">Prochaines Dates...</a>


<%
const now = (new Date()).getTime();
const futureEvents = events.filter( (e) => e.dateEnd.getTime()>now && e.type.titleSlug === eventFilterSlug );
futureEvents.sort( (a,b) => a.dateStart.getTime() - b.dateStart.getTime() );
futureEvents.forEach(event=>{
_%>
<%- include('../fragments/event.ejs',{myEvent:event}); %>
<% }); _%>
