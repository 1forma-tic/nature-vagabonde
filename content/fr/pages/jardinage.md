title: Jardinage
<<: !!inc/file ../temoignages.yml
-------------------
# Jardinage

<img src="img/Jardinage1.jpg" class="portrait"/>


Nous aimons faire découvrir dans les jardins potagers plusieurs applications des principes de la permaculture et du jardinage naturel, tels que :

- sol toujours couvert de végétaux vivants ou en compostage de surface
- amendement végétal comme en forêt et avec du compost
- respect et encouragement de la vie du sol (arthropodes, bactéries, champignons, nématodes,….)
- recherche d’économie d’eau
- accueil du sauvage qui augmente la résilience des plantes potagères, et le potentiel d’interactions bénéfiques entre elles
- prendre le temps d’observer, de ressentir le jardin et son environnement
- valoriser la diversité
- embellir tout en favorisant la production de légumes
- densification et association des cultures
-récolter ses graines qui ont engrammé les informations du sol

Vous pouvez avoir un aperçu de notre coopération avec la nature au petit jardinet de la biocoop de Gradignan.



# Grainothèque

La mise en place d'une boite de graines à partager est à l'étude. 

# Actualité au jardin

<%
const htmlAlbum = [];
htmlAlbum.push(`<div class="card ratio-3-4 avecTitre"><img src="img/chou_rave1.jpg"/><span>chou rave</span></div>`);
htmlAlbum.push(`<div class="card ratio-3-4 avecTitre"><img src="img/graine_poiree1.jpg"/><span>graines de  poirée</span></div>`);
htmlAlbum.push(`<div class="card ratio-3-4 avecTitre"><img src="img/JardinBiocoop1.jpg"/><span>jardin de la Biocoop</span></div>`);
htmlAlbum.push(`<div class="card ratio-3-4 avecTitre"><img src="img/spynxDuLiseron1.jpg"/><span>spynx du liseron</span></div>`);
htmlAlbum.push(`<div class="card ratio-3-4 avecTitre"><img src="img/larveCoccinelle1.jpg"/><span>larve de coccinelle</span></div>`);
%>
<%- include('../../../node_modules/1gallery/1gallery.ejs',{cards:htmlAlbum,classes:'galerieQui'}) %>


