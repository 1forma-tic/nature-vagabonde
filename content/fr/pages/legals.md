title: Mentions légales
-------------------
# Mentions légales

<img src="img/formation.jpg" class="portrait"/>

## Responsable éditorial

Hélène Brard
<br/>contact naturevagabonde@riseup.net
<br/>Gironde France

## Responsable technique et hébergement web
<address>
Millicent Billette - 1forma-tic.fr
<br/>26 Ter, rue André Lapelleterie, 33130, Bègles
<br/><a href="tel:0033770772770">+33&nbsp;(0)&nbsp;770&nbsp;772&nbsp;770</a>
<br/><a href="mailto:contact@1forma-tic.fr?subject=[legals]">contact@1forma-tic.fr</a>
<br/>SIRET : <a href="https://www.societe.com/societe/m-millicent-billette-de-villemeur-520193897.html"
                target="_blank">520&nbsp;193&nbsp;897&nbsp;00037</a>
</address>

## Politique de confidentialité

Pas de cookies sur ce site,
ni de <a href="https://blogantipub.wordpress.com/charte-des-sites-sans-pub/" target="_blank">publicités</a> ou d'indiscrétions de notre part
ni de porte d'accès pour des tiers.

Ce site respecte votre vie privée.

Mon hébergeur considère l'usage de logs serveur anonymisés comme une trace
plus que suffisante pour évaluer la fréquentation globale du site
et y adapter son infrastructure.
Pour que la navigation sur le site reste fluide,
mon hébergeur ajuste son infrastructure
en fonction, entre autre, de la fréquentation du site.
Cette fréquentation est mesurée anonymement a partir des logs serveur,
ainsi, seul le minimum de trace est conservé, et aucun trafic réseau
supplémentaire n'est généré (+ rapide et + écologique).

Le site étant physiquement hébergé dans ses locaux,
ces logs serveur ne sont accessibles à aucun tiers.



## Crédits et licences

Toutes les photos ont été réalisées par l'association. Elle sont mises à disposition sous licence :
<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr" target="_blank">CC-BY-NC-SA</a>.

Le <a href="https://framagit.org/1forma-tic/nature-vagabonde/" target="_blank">code source du site</a>
est sous licence
<a href="https://creativecommons.org/publicdomain/zero/1.0/deed.fr" target="_blank">CC-0</a>.
