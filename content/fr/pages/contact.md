title: Contact

<<: !!inc/file ../partenaires.yml
-------------------

# Qui sommes nous ?

<%
const htmlAlbum = [];
htmlAlbum.push(`<div class="card ratio-2-3 avecTitre"><img src="img/Valérie.jpg"/><span>Valérie</span></div>`);
htmlAlbum.push(`<div class="card ratio-2-3 avecTitre"><img src="img/Béa.jpg"/><span>Béa</span></div>`);
htmlAlbum.push(`<div class="card ratio-2-3 avecTitre"><img src="img/Eric.jpg"/><span>Eric</span></div>`);
htmlAlbum.push(`<div class="card ratio-2-3 avecTitre"><img src="img/Hélène.jpg"/><span>Hélène</span></div>`);
%>
<%- include('../../../node_modules/1gallery/1gallery.ejs',{cards:htmlAlbum,classes:'galerieQui'}) %>


# Nos coordonnées



Courriel : naturevagabonde@riseup.net



# Quelques sites "Amis" 


<hr/>

<%- include('../fragments/partenaires.ejs'); %>
