title: Fêtes des cycles de la nature
<<: !!inc/file ../dates.yml
eventFilterSlug: fêtes
-------------------
# Fêtes des cycles de la nature

<img src="img/fete1.jpg" class="portrait"/>

Nous nous réunissons pour célébrer les moments de changements dans le cycle de la nature, le rythme des saisons. Sans ésotérisme, nous recherchons dans chaque fête les transformations visibles de la nature et qui font échos en nous.

Nous installons un autel représentant les éléments significatifs de chaque fête, nous nous relions et expérimentons une balade sensorielle et méditative en forêt. Nous créons un objet symbole par une activité artisanale.	

Nous partageons un repas convivial et de saison.

<br>
<br>

<a class="button right withPicto agenda" style="margin-right: 30px" href="dates.html">Prochaines Dates...</a>

<%
const now = (new Date()).getTime();
const futureEvents = events.filter( (e) => e.dateEnd.getTime()>now && e.type.titleSlug === eventFilterSlug );
futureEvents.sort( (a,b) => a.dateStart.getTime() - b.dateStart.getTime() );
futureEvents.forEach(event=>{
_%>
<%- include('../fragments/event.ejs',{myEvent:event}); %>
<% }); _%>
