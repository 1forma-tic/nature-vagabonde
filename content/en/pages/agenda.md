title: Dates
-------------------

# Dates


## Evénements que je recommande :
- 16 et 17 mai, [Festival Cococo&co](https://cococo-et-co.com) au site du Bourgailh à Bordeaux

## Stages d'initiation :
Tous les stages d'initiation sont à prix (presque) conscient et nécessitent pour l'inscription 50€ d’acompte et le remplissage du formulaire. L'hébergement est à régler directement avec l'organisme d'accueil.

- 23-24 mai au [Petit Pontbiel](https://www.petitpontbiel.com/) Pontonx-sur-Adour (Landes - 40) : [Je m'inscris !](https://framaforms.org/inscription-au-stage-dinitiation-a-la-permaculture-mai-2020-petit-pontbiel-1582651926)
- 27-28 juin à l'[écolieu Cablanc](https://www.cablanc.com/accueil/cablanc-lecotourisme-au-coeur-des-vignobles-ecotourisme-cablanc-dordogne/), Saussignac (Dordogne - 24) : [Je m'inscris !](https://framaforms.org/inscription-au-stage-dinitiation-a-la-permaculture-juin-2020-ecolieu-cablanc-1583089617)
- 4-5 juillet au [Baccara Lodge](https://www.baccaralodge.fr/), Morceinx (Landes - 40) : [Je m'inscris !](https://framaforms.org/inscription-au-stage-dinitiation-a-la-permaculture-juillet-2020-baccara-lodge-1583106714)

## CCP Cours de Conception en Permaculture Kit de survie pour inventer demain
- du 16 au 31 août au [Petit Pontbiel](https://www.petitpontbiel.com/) (Landes - 40) [Lire la brochure (pdf)](img/CCP-Petit-Pontbiel-2020-Cours-Certifie-Permaculture.pdf) : [Je m'inscris !](https://framaforms.org/inscription-au-ccp-2020-kit-de-survie-pour-co-construire-demain-1580987260)

[![Dossier CCP](img/CCPPetitPontbiel2020-page-de-garde.jpg)](img/CCP-Petit-Pontbiel-2020-Cours-Certifie-Permaculture.pdf)
